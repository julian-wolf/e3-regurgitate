#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <signal.h>
#include <stdbool.h>
#include <getopt.h>

#include <sys/time.h>

#include "redpitaya/rp.h"

#include "rp_constants.h"
#include "utils.h"

#define CONSTANTLY_READ_AND_OUTPUT 0

static int pin_AI = 0;
static int pin_AO = 0;
static int i_trig_record_pin = 6;
static int i_trig_output_pin = 7;

static int do_use_fast = 0;

int main (int argc, char **argv) {
	// Define required arguments
	int waveform_length_ms = INVALID_ARGUMENT_INPUT;
	int sample_rate_hz = INVALID_ARGUMENT_INPUT;
	
	// Define optional argumentns
	int trigger_holdoff_ms = 0;
	int n_running_averages = 1;
	int verbose = 0;
	float valid_range_low = INVALID_ARGUMENT_INPUT;
	float valid_range_high = INVALID_ARGUMENT_INPUT;
	
	static struct option long_options[] =
		{	{"waveform-length-ms",	required_argument,	0,	'l'}
		,	{"sample-rate-hz",		required_argument,	0,	'f'}
		,	{"trigger-holdoff-ms",	required_argument,	0,	'w'}
		,	{"n-running-averages",	required_argument,	0,	'n'}
		,	{"verbose",				optional_argument,	0,	'v'}
		,	{"valid-range-min",		required_argument,	0,	'a'}
		,	{"valid-range-max",		required_argument,	0,	'b'}
		,	{"use-fast",			no_argument,		&do_use_fast,	1}
		,	{"help",				no_argument,		0,	'h'}
		};
	
	// Parse arguments
	int c;
	int option_index = 0;
	while (-1 != (c = getopt_long(argc, argv, "l:f:w:n:v::a:b:yh", long_options, &option_index))) {
		switch (c) {
			case 0:
				if (NULL != optarg) {
					fprintf(stderr, "Option --%s does not accept arguments."
						"See ./regurgatate -h for options.\n", long_options[option_index].name);
					return EXIT_FAILURE;
				}

				break;

			case 'l':
				waveform_length_ms = atoi(optarg);
				break;
			
			case 'f':
				sample_rate_hz = atoi(optarg);
				break;
			
			case 'w':
				trigger_holdoff_ms = atoi(optarg);
				break;
			
			case 'n':
				n_running_averages = atoi(optarg);
				break;
			
			case 'v':
				verbose = (NULL != optarg) ? atoi(optarg) : 1;
				break;

			case 'a':
				valid_range_low = atof(optarg);
				break;

			case 'b':
				valid_range_high = atof(optarg);
				break;
			
			case 'y':
				// Flag has already been set by getopt_long()
				break;
			
			case 'h':
				print_help();
				return EXIT_FAILURE;
				break;
			
			case '?':
		//		if ('l' == optopt || 'f' == optopt || 'w' == optopt || 'n' == optopt || 'a' == optopt || 'b' == optopt) {

		//			fprintf(stderr, "Option -%c requires an argument. See `./regurgitate -h` for options.\n", optopt);
		//		}
		//		else if (isprint(optopt)) {
		//			fprintf (stderr, "Unknown option `-%c'.\n", optopt);
		//		}
		//		else {
		//			fprintf (stderr, "Unknown option character `\\x%x'.\n", optopt);
		//		}
		//		
		//		return EXIT_FAILURE;

				break;
			
			default:
				abort();
		}
	}
	
	// Ensure required arguments have been supplied
	if (INVALID_ARGUMENT_INPUT == waveform_length_ms) {
		fprintf(stdout, "Missing required argument -l. See `./regurgitate -h` for options.\n");
		return EXIT_FAILURE;
	}
	
	if (INVALID_ARGUMENT_INPUT == sample_rate_hz) {
		fprintf(stdout, "Missing required argument -f. See `./regurgitate -h` for options.\n");
		return EXIT_FAILURE;
	}

	if (verbose >= 2) {
		printf(	"Received options:\n"
			"\twaveform_length_ms = %d\n"
			"\tsample_rate_hz = %d\n"
			"\ttrigger_holdoff_ms = %d\n"
			"\tn_running_averages = %d\n"
			"\tverbose = %d\n"
			"\tdo_use_fast = %d\n",
			waveform_length_ms, sample_rate_hz, trigger_holdoff_ms, n_running_averages, verbose, do_use_fast);
	}
	
	// Define derived input parameters
	int sample_time_us = 1000000 / sample_rate_hz;
	int trigger_holdoff_us = 1000 * trigger_holdoff_ms;

    	// Initialization of API
	if (verbose >= 2) {
		fprintf(stdout, "Initializing Red Pitaya API.\n");
	}

	if (RP_OK != rp_Init()) {
		fprintf(stderr, "Red Pitaya API init failed!\n");
		return EXIT_FAILURE;
	}
	
	// Allow for exiting cleanly when Ctrl-C is received
	setup_keyboard_interrupt_handler();
	signal(SIGINT, &keyboard_interrupt_handler);

	// Configure DIO[0:7]_N to inputs
	if (verbose >= 2) {
		fprintf(stdout, "Preparing digital pins to accept input.\n");
	}

	for (int i = 0; i < 8; ++i) {
		rp_DpinSetDirection (i+RP_DIO0_N, RP_IN);
	}
		
	bool shot_is_valid[n_running_averages];
	for (int i = 0; i < n_running_averages; ++i) {
		shot_is_valid[i] = false;
	}

	int i_avg = 0; // current meta-index being averaged into

	rp_pinState_t trig_record_state;
	rp_pinState_t trig_output_state;

	if (do_use_fast) {
		if (waveform_length_ms > 1000) {
			fprintf(stderr, "Fast acquisition (option -y) is only available for waveform lengths < 1 s.\n");
		}

		unsigned int waveform_n_samples = 16384;
		rp_acq_decimation_t decimation = 8192;
		
		float **waveform = malloc(sizeof *waveform * n_running_averages); // recent single-shot waveforms
		float *waveform_avg = malloc(sizeof *waveform_avg * waveform_n_samples); // running-average waveform
		initialize_waveforms(waveform, waveform_avg, waveform_n_samples, n_running_averages);

		rp_AcqReset();
		rp_AcqSetDecimation(decimation);
		rp_AcqSetTriggerSrc(RP_TRIG_SRC_DISABLED);
		rp_AcqSetTriggerDelay(0);

		rp_AcqStart();
		sleep(1);

		rp_GenWaveform(RP_CH_1, RP_WAVEFORM_ARBITRARY);
		rp_GenArbWaveform(RP_CH_1, waveform_avg, waveform_n_samples);
		rp_GenAmp(RP_CH_1, 1.0);

		rp_GenMode(RP_CH_1, RP_GEN_MODE_BURST);
		rp_GenBurstCount(RP_CH_1, 1);
		rp_GenBurstRepetitions(RP_CH_1, 1);
		rp_GenOutEnable(RP_CH_1);
		rp_GenTriggerSource(RP_CH_1, RP_GEN_TRIG_SRC_INTERNAL);

		if (verbose >= 1) {
			fprintf(stdout, "Entering main program loop.\n");
		}

		while (!_received_keyboard_interrupt) {
			rp_DpinGetState(i_trig_record_pin+RP_DIO0_N, &trig_record_state);
			if (RP_HIGH == trig_record_state) {
				rp_AcqSetTriggerSrc(RP_TRIG_SRC_NOW);
				sleep(1);
				rp_AcqGetOldestDataV(RP_CH_2, &waveform_n_samples, waveform[i_avg]);

				float curr_waveform_mean = 0;
				for (int j = 0; j < waveform_n_samples; ++j) {
					curr_waveform_mean += waveform[i_avg][j];
				}
				curr_waveform_mean /= waveform_n_samples;

				shot_is_valid[i_avg] = true;
				if (INVALID_ARGUMENT_INPUT != valid_range_low && curr_waveform_mean < valid_range_low) {
					shot_is_valid[i_avg] = false;
					
					if (verbose >= 1) {
						fprintf(stdout, "Shot failed varifier: %.3f < valid_range_low.\n", curr_waveform_mean);
					}
				}
				else if (INVALID_ARGUMENT_INPUT != valid_range_high && curr_waveform_mean > valid_range_high) {
					shot_is_valid[i_avg] = false;

					if (verbose >= 1) {
						fprintf(stdout, "Shot failed varifier: %.3f > valid_range_high.\n", curr_waveform_mean);
					}
				}
				
				for (int j = 0; j < waveform_n_samples; ++j) {
					float avg = 0;
					int n_valid_shots = 0;
					for (int i = 0; i < n_running_averages; ++i) {
						if (shot_is_valid[i]) {
							avg += waveform[i][j];
							++n_valid_shots;
						}
					}
					avg /= n_valid_shots;
					waveform_avg[j] = avg;
				}

				if (verbose >= 2) {
					fprintf(stdout, "Mean reading: %.3f V.\n", curr_waveform_mean);
				}

				i_avg = (i_avg + 1) % n_running_averages;
				
				continue;
			}

			rp_DpinGetState(i_trig_output_pin+RP_DIO0_N, &trig_output_state);
			if (RP_HIGH == trig_output_state) {
				int status = rp_GenTrigger(1);
				if (RP_OK != status) {
					fprintf(stderr, "Unable to generate output trigger.");
					break;
				}

				continue;
			}
		}

		free(waveform);
		free(waveform_avg);
	}
	else {
		int waveform_n_samples = 1000 * waveform_length_ms / sample_time_us;

		float **waveform = (float **) malloc(sizeof(float *) * n_running_averages);  // recent single-shot waveforms
		float *waveform_avg = (float *) malloc(sizeof(float) * waveform_n_samples); // running-average waveform
		initialize_waveforms(waveform, waveform_avg, waveform_n_samples, n_running_averages);

		struct timeval time_prev;
		struct timeval time_curr;
		struct timeval time_start;

		gettimeofday(&time_prev, NULL);
		unsigned long time_prev_us = tv_to_us(&time_prev);
		unsigned long time_curr_us;
		
		bool do_record = false; // current state of the "record" trigger
		bool do_output = false; // current state of the "output" trigger
		
		if (verbose >= 1) {
			fprintf(stdout, "Entering main program loop.\n");
		}

		while (!_received_keyboard_interrupt) {
#if CONSTANTLY_READ_AND_OUTPUT
			gettimeofday(&time_curr, NULL);
			time_curr_us = tv_to_us(&time_curr);
			if (time_curr_us - time_prev_us >= sample_time_us) {
				float val_curr;
				rp_AIpinGetValue(pin_AI, &val_curr);

				if (verbose >= 1) {
					fprintf(stdout, "Read %.3f V from AI[%i]. Writing to AO[%i].\n",
						val_curr, pin_AI, pin_AO);
				}

				int status = rp_AOpinSetValue(pin_AO, val_curr);
				if (RP_OK != status) {
					fprintf(stderr, "Unable to set AO[%i] voltage.\n", pin_AO);
					break;
				}
				
				time_prev_us = time_curr_us;
			}

			continue;
#endif

			if (do_record) {
				if (verbose >= 1) {
					fprintf(stdout, "Received `record` trigger.\n");

					gettimeofday(&time_start, NULL);
				}

				int i_sample = 0;
				while (i_sample < waveform_n_samples) {
					gettimeofday(&time_curr, NULL);
					time_curr_us = tv_to_us(&time_curr);
					if (time_curr_us - time_prev_us >= sample_time_us) {
						rp_AIpinGetValue(pin_AI, &waveform[i_avg][i_sample]);
						
						time_prev_us = time_curr_us;
						++i_sample;
					}
				}

				if (verbose >= 1) {
					gettimeofday(&time_curr, NULL);
					float t_elapsed_ms = 1000.0 * (time_curr.tv_sec - time_start.tv_sec)
							+ 0.001 * (time_curr.tv_usec - time_start.tv_usec);

					fprintf(stdout, "Finished recording. Elapsed time: %.1f ms.\n", t_elapsed_ms);
				}

				float curr_waveform_mean = 0;
				for (int j = 0; j < waveform_n_samples; ++j) {
					curr_waveform_mean += waveform[i_avg][j];
				}
				curr_waveform_mean /= waveform_n_samples;

				shot_is_valid[i_avg] = true;
				if (INVALID_ARGUMENT_INPUT != valid_range_low && curr_waveform_mean < valid_range_low) {
					shot_is_valid[i_avg] = false;
					
					if (verbose >= 1) {
						fprintf(stdout, "Shot failed varifier: %.3f < valid_range_low.\n", curr_waveform_mean);
					}
				}
				else if (INVALID_ARGUMENT_INPUT != valid_range_high && curr_waveform_mean > valid_range_high) {
					shot_is_valid[i_avg] = false;

					if (verbose >= 1) {
						fprintf(stdout, "Shot failed varifier: %.3f > valid_range_high.\n", curr_waveform_mean);
					}
				}
				
				for (int j = 0; j < waveform_n_samples; ++j) {
					float avg = 0;
					int n_valid_shots = 0;
					for (int i = 0; i < n_running_averages; ++i) {
						if (shot_is_valid[i]) {
							avg += waveform[i][j];
							++n_valid_shots;
						}
					}
					avg /= n_valid_shots;
					waveform_avg[j] = avg;
				}

				if (verbose >= 2) {
					fprintf(stdout, "Mean reading: %.3f V.\n", curr_waveform_mean);
				}
				
				i_avg = (i_avg + 1) % n_running_averages;
			
				do_record = false;
			}
			else if (do_output) {
				if (verbose >= 1) {
					fprintf(stdout, "Received `output` trigger.\n");
					
					gettimeofday(&time_start, NULL);
				}

				int i_sample = 0;
				while (i_sample < waveform_n_samples) {
					gettimeofday(&time_curr, NULL);
					time_curr_us = tv_to_us(&time_curr);
					if (time_curr_us - time_prev_us >= sample_time_us) {
						int status = rp_AOpinSetValue(pin_AO, waveform_avg[i_sample]);
						if (RP_OK != status) {
							fprintf(stderr, "Unable to set AO[%i] voltage.\n", pin_AO);
							break;
						}
						
						time_prev_us = time_curr_us;
						++i_sample;
					}
				}
				
				if (verbose >= 1) {
					gettimeofday(&time_curr, NULL);
					float t_elapsed_ms = 1000.0 * (time_curr.tv_sec - time_start.tv_sec)
							+ 0.001 * (time_curr.tv_usec - time_start.tv_usec);

					fprintf(stdout, "Finished outputting. Elapsed time: %.1f ms.\n", t_elapsed_ms);
				}

				do_output = false;
			}
			else {
				int status = rp_AOpinSetValue(pin_AO, rp_constants.zero_volts);
				if (RP_OK != status) {
					fprintf(stderr, "Unable to set AO[%i] voltage.\n", pin_AO);
					break;
				}
			}
			
			gettimeofday(&time_curr, NULL);
			time_curr_us = tv_to_us(&time_curr);
			if (time_curr_us - time_prev_us < trigger_holdoff_us) {
				continue;
			}
			
			if (!do_record) {
				rp_DpinGetState(i_trig_record_pin+RP_DIO0_N, &trig_record_state);
				do_record = trig_record_state;
			}
			
			if (!do_output) {
				rp_DpinGetState(i_trig_output_pin+RP_DIO0_N, &trig_output_state);
				do_output = trig_output_state;
			}
		}	

		free(waveform);
		free(waveform_avg);
	}

	fprintf(stdout, "\nReceived Ctrl-C. Exiting cleanly.\n");

	// Releasing resources
	rp_Release();

	return EXIT_SUCCESS;
}
