# Description

TODO

# Usage

In order to use the API, a specific FPGA image may need to be loaded:
```bash
cat /opt/redpitaya/fpga/fpga_0.94.bit > /dev/xdevcfg
```

In order to run the executable, the path to the Red Pitaya shared libraries may need to be provided explicitly:
```bash
LD_LIBRARY_PATH=/opt/redpitaya/lib ./regurgitate
```

