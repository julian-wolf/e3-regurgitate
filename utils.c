#include "utils.h"

void print_help() {
	fprintf(stdout,
		"Usage: LD_LIBRARY_PATH=/opt/redpitaya/lib ./regurgitate"
		" -l waveform-length-ms"
		" -f sample-rate-hz"
		" [-w trigger-holdoff-ms]"
		" [-n n-running-averages]"
		" [-v verbose]"
		" [-a min-valid-input]"
		" [-b max-valid-input]"
		" [-y]"
		" [-h]"
		"\n\n"
		"Options:\n"
		"\tTODO"
		"\n"
	);
}

void setup_keyboard_interrupt_handler() {
	_received_keyboard_interrupt = false;
}

void keyboard_interrupt_handler(int signum) {
	_received_keyboard_interrupt = true;
}

unsigned long tv_to_us(struct timeval *tv) {
	return 1000000 * tv->tv_sec + tv->tv_usec;
}

void initialize_waveforms(float **waveform, float *waveform_avg, int waveform_n_samples, int n_running_averages) {
	for (int i = 0; i < n_running_averages; ++i) {
		waveform[i] = malloc(sizeof *waveform[i] * waveform_n_samples);
		for (int j = 0; j < waveform_n_samples; ++j) {
			waveform[i][j] = 0;
		}
	}
	
	for (int i = 0; i < waveform_n_samples; ++i) {
		waveform_avg[i] = 0;
	}
}

