CC = /usr/bin/gcc
CFLAGS  = -g -std=gnu99 -Wall -Werror
CFLAGS += -I/opt/redpitaya/include
LDFLAGS = -L/opt/redpitaya/lib
LDLIBS = -lm -lpthread -lrp

all: regurgitate

%.o: %.c 
	$(CC) -c $(CFLAGS) $< -o $@

regurgitate: regurgitate.o utils.o 
	$(CC) $(CFLAGS) $^ $(LDFLAGS) $(LDLIBS) -o $@

clean:
	$(RM) *.o

