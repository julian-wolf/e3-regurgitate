#ifndef _UTILS_H
#define _UTILS_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

enum {
	INVALID_ARGUMENT_INPUT = -1
};

void print_help(); 

bool _received_keyboard_interrupt;
void setup_keyboard_interrupt_handler(void);
void keyboard_interrupt_handler(int signum);

unsigned long tv_to_us(struct timeval *tv);

void initialize_waveforms(float **waveform, float *waveform_avg, int waveform_n_samples, int n_running_averages);

#endif
